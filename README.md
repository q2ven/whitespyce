# How to use

If you need to run program in debug mode, add one more argument.

```
$ python3 interpreter.py samples/helloworld.ws
HELLO WORLD
$ python3 interpreter.py samples/helloworld.ws debug
...  # STEP EXECUTION
```

## 経緯
研究室の勉強会でWhitespaceを書く機会があったが、公式のサイトが無いので、公式のインタプリタを取得できなかった。([アーカイブ](https://web.archive.org/web/20170318183338/http://compsoc.dur.ac.uk/whitespace/tutorial.html)でドキュメントは確認できた。)

また、公開されていた[オンラインIDE](https://whitespace.kauaveel.ee/)ではJump Negativeが動作しないバグが存在する。

のでインタプリタを作成した。