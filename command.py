class Command(object):
    cmds = {}

    def __init__(self, context):
        self.context = context

    @classmethod
    def interpret(cls, context):
        instance = cls(context)
        token = context.get_token()

        cmd_name = instance.cmds[token]

        if context.debug:
            input('\n{:14}:\t{}'.format(
                cmd_name,
                instance.coloured_token(),
            ))
            if cmd_name.startswith('print'):
                print('>> ', end='')

        result = getattr(instance, cmd_name)()

        if context.debug:
            if cmd_name.startswith('print'):
                print()
            print('stack\t: ', context.stack)
            print('heap\t: ', context.heap)

        return result

    def coloured_token(self):
        red = '\x1b[6;30;41m'
        green = '\x1b[6;30;42m'
        yellow = '\x1b[6;30;43m'
        end = '\x1b[0m'

        colour_map = {
            ' ': red,
            '\t': green,
            '\n': yellow,
        }

        char_map = {
            ' ': ' ',
            '\t': '        ',
            '\n': '  ',
        }

        string = ''

        index = self.context.index
        token = self.context.tokens[index - 2] + self.context.tokens[index - 1]

        for c in token:
            string += '|'
            string += colour_map[c] + char_map[c]
            string += end
        string += '|'

        return string + end


class StackCommand(Command):
    cmds = {
        ' ': 'push',
        '\n ': 'duplicate',
        '\n\t': 'swap',
        '\n\n': 'discard',
    }

    def push(self):
        param = self.context.get_token().rstrip('\n')
        sign = 1 if param[0] == ' ' else -1
        self.context.push(
            sign * int(
                param[1:].replace(' ', '0').replace('\t', '1'),
                2
            )
        )

    def duplicate(self):
        obj = self.context.pop()
        self.context.push(obj)
        self.context.push(obj)

    def swap(self):
        a = self.context.pop()
        b = self.context.pop()
        self.context.push(a)
        self.context.push(b)

    def discard(self):
        self.context.pop()


class ArithmeticCommand(Command):
    cmds = {
        '  ': 'add',
        ' \t': 'sub',
        ' \n': 'mul',
        '\t ': 'div',
        '\t\t': 'mod',
    }

    def add(self):
        a = self.context.pop()
        b = self.context.pop()
        self.context.push(b + a)

    def sub(self):
        a = self.context.pop()
        b = self.context.pop()
        self.context.push(b - a)

    def mul(self):
        a = self.context.pop()
        b = self.context.pop()
        self.context.push(b * a)

    def div(self):
        a = self.context.pop()
        b = self.context.pop()
        self.context.push(b // a)

    def mod(self):
        a = self.context.pop()
        b = self.context.pop()
        self.context.push(b % a)


class HeapCommand(Command):
    cmds = {
        ' ': 'store',
        '\t': 'retrieve',
    }

    def store(self):
        value = self.context.pop()
        address = self.context.pop()
        self.context.store(address, value)

    def retrieve(self):
        address = self.context.pop()
        value = self.context.get(address)
        self.context.push(value)


class FlowCommand(Command):
    cmds = {
        '  ': 'mark',
        ' \t': 'call',
        ' \n': 'jump',
        '\t ': 'jump_zero',
        '\t\t': 'jump_negative',
        '\t\n': 'control_back',
        '\n\n': 'end',
    }

    def mark(self):
        self.context.get_token()

    def call(self):
        label = self.context.get_token()
        self.context.call(label)

    def jump(self):
        label = self.context.get_token()
        self.context.jump(label)

    def jump_zero(self):
        label = self.context.get_token()
        if self.context.pop() == 0:
            self.context.jump(label)

    def jump_negative(self):
        label = self.context.get_token()
        if self.context.pop() < 0:
            self.context.jump(label)

    def control_back(self):
        self.context.control_back()

    def end(self):
        self.context.end = True


class IOCommand(Command):
    cmds = {
        '  ': 'printc',
        ' \t': 'printi',
        '\t ': 'readc',
        '\t\t': 'readi',
    }

    def printc(self):
        print(chr(self.context.pop()), end='')

    def printi(self):
        print(self.context.pop(), end='')

    def readc(self):
        self.context.store(
            self.context.pop(),
            ord(input('<< '))
        )

    def readi(self):
        self.context.store(
            self.context.pop(),
            int(input('<< '))
        )
