import re


class Context(object):
    syntax = {
        # Stack
        ' ': {
            ' ': r'^[ \t]+?\n',  # push
            '\n ': r'',  # duplicate
            '\n\t': r'',  # swap
            '\n\n': r'',  # discard
        },
        # Arithmetic
        '\t ': {
            '  ': r'',  # add
            ' \t': r'',  # sub
            ' \n': r'',  # mul
            '\t ': r'',  # div
            '\t\t': r'',  # mod
        },
        # Heap
        '\t\t': {
            ' ': r'',  # store
            '\t': r'',  # retrieve
        },
        # Flow
        '\n': {
            '  ': r'^[ \t]+?\n',  # mark
            ' \t': r'^[ \t]+?\n',  # call
            ' \n': r'^[ \t]+?\n',  # jump
            '\t ': r'^[ \t]+?\n',  # jump zero
            '\t\t': r'^[ \t]+?\n',  # jump negative
            '\t\n': r'',  # control back
            '\n\n': r'',  # end
        },
        # I/O
        '\t\n': {
            '  ': r'',  # printc
            ' \t': r'',  # printi
            '\t ': r'',  # readc
            '\t\t': r'',  # readi
        }
    }

    def __init__(self, code):
        self.code = code
        self.index = 0

        self.tokens = []
        self.indexes = []
        self.labels = {}

        if not self.check():
            raise SyntaxError

        # reset index
        self.index = 0
        self.stack = []
        self.heap = {}
        self.caller = []
        self.debug = False

    def check(self):
        while True:
            if not self.code[self.index:]:
                break

            if not self.check_imp():
                return False

        return True

    def check_imp(self):
        for imp in self.syntax:
            if self.code[self.index:].startswith(imp):
                self.indexes.append(self.index)
                self.tokens.append(imp)
                self.index += len(imp)
                return self.check_cmd(imp)
        return False

    def check_cmd(self, imp):
        for cmd in self.syntax[imp]:
            if self.code[self.index:].startswith(cmd):
                self.indexes.append(self.index)
                self.tokens.append(cmd)
                self.index += len(cmd)
                return self.check_param(imp, cmd)
        return False

    def check_param(self, imp, cmd):
        match = re.match(self.syntax[imp][cmd], self.code[self.index:])
        if match:
            self.index += match.end()
            matched = match.group()
            if matched:
                self.indexes.append(self.index)
                self.tokens.append(matched)
                if imp == '\n':
                    self.labels[matched] = self.index
            return True
        return False

    def get_token(self):
        token = self.tokens[self.index]
        self.index += 1
        return token

    def push(self, obj):
        self.stack.append(obj)

    def pop(self):
        return self.stack.pop(-1)

    def store(self, address, value):
        self.heap[address] = value

    def get(self, address):
        return self.heap[address]

    def call(self, label):
        self.caller.append(self.index)
        self.jump(label)

    def jump(self, label):
        self.index = self.indexes.index(self.labels[label])
        self.index += 1

    def control_back(self):
        self.index = self.caller.pop(-1)
