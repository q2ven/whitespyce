import sys

from command import (ArithmeticCommand, FlowCommand, HeapCommand, IOCommand,
                     StackCommand)
from context import Context


class Whitespace(object):
    imps = {
        ' ': StackCommand,
        '\t ': ArithmeticCommand,
        '\t\t': HeapCommand,
        '\n': FlowCommand,
        '\t\n': IOCommand,
    }

    def __init__(self, code):
        self.context = Context(code)

    def run(self):
        self.interpret()

    def step(self):
        self.context.debug = True
        self.interpret()

    def interpret(self):
        if self.context.debug:
            print('*' * 10 + ' Debug Mode ' + '*' * 10, end='')

        while True:
            token = self.context.get_token()

            if not token:
                break

            self.imps[token].interpret(self.context)

            if getattr(self.context, 'end', None):
                if not self.context.debug:
                    print()
                return True
        return False


if __name__ == '__main__':
    f = open(sys.argv[1])
    if len(sys.argv) == 3:
        debug = True
    else:
        debug = False

    if debug:
        Whitespace(f.read()).step()
    else:
        Whitespace(f.read()).run()
