def num_to_ws(number):
    return ' {:b}\n'.format(
        number
    ).replace('0', ' ').replace('1', '\t')


string = 'HELLO WORLD'[::-1]  # reverse string

whitespace = []

for c in string:
    whitespace.append('  ')  # push
    whitespace.append(num_to_ws(ord(c)))  # char

whitespace.append('\t\n  ' * len(string))  # printc
whitespace.append('\n\n\n')  # end of program

print(''.join(whitespace), end='')
